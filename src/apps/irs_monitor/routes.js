import view from './pages/dashboard.vue'

export default [
  {
    path: '/irs/monitor',
    component: view,
    name: 'irs_monitor'
  }
]
