import tasker from './pages/tasker.vue'

export default [
  {
    path: '/irs/tasker',
    component: tasker,
    name: 'irs_tasker'
  }
]
