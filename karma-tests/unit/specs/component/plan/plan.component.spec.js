xdescribe('plan.vue', () => {

  describe("create new plan", () => {
    it('can load all available spatial_hierarchies', () => {

    })

    it('can display selected spatial_heriarchy', () => {})
  })
  describe("view existing plan", () => {})
  describe("edit existing plan", () => {})
  describe("clear existing plan", () => {})


  it('should get latest plan from remote', () => {})

  it('should get village-structure-clusters from remote', () => {})

  it('should calculate the number of days to spray from the inputs given', () => {})

  it('should calculate the correct risk scale', () => {})

  it("should set 'unsaved_changes' be true after making a change", () => {})

  it('should load remote plan when "Cancel edits"', () => {})

  it("should clear the plan when 'Clear Plan' is clicked", () => {})

  it("should save the plan when 'Save' is clicked", () => {})

  it('should show controls when in edit mode', () => {})

  // MAP
  it('should show areas by risk when selected', () => {})

  // TABLE
  it('should display all selected areas in the table', () => {})

})
